## Übungsaufgaben zu Representational State Transfer (ReST)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111336676803320765</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/rest-uebung</span>

> **tl/dr;** _(ca. 30 min Bearbeitungszeit): Aufgaben zu den Grundlagen von ReST: Zunächst ein paar allgemeine Leitfragen zu Begriffen und Konzepten, dann zwei Fallbeispiele (API einer smarten Heizung und eines Industrieroboters), anhand derer das Grundlagenverständnis für ReST-Schnittstellen abgeprüft werden kann._

Sämtliche Fragen beziehen sich auf die ReST-Grundlagen, wie sie in [diesem Artikel](https://oer-informatik.de/rest-grundlagen) beschrieben sind.

### 1. Leitfragen zu ReST-APIs

1. Was wird im ReST-Kontext unter einer Ressource verstanden?

1. Was versteht man im Rahmen von ReST unter einer Represenation?

1. Was unterscheidet eine URL von einer URI und von einer URN? Nenne jeweils Beispiele!

1. Welche HTTP-Methoden realisieren jeweils die vier Verantwortlichkeiten einer `CRUD`-Schnittstelle?

1. Welche Garantien bieten die HTTP-Methoden für `CRUD`-Operationen?

1. Welche Aufgabe übernimmt die HTTP-Methode `HEAD`?

1. Was versteht ReST unter dem Begriff "Hypermedia"?

1. ReST-APIs antworten auf Requests immer auch mit Statuscodes (wie alle HTTP-Responses). Wie lassen sich die Gruppen der 100er, 200er, 300er, 400er und 500er Statuscodes jeweils bezeichnen (mit Verweis auf die Eigenschaft der Reaktion der API)?

### 2. ReST-API für eine smarte Heizungsanlage

Die Steuerung einer Heizung bietet einen ReST-Service, der vom Raumthermostat zum Einlesen und Schreiben von Daten genutzt wird. Ein Kollege übergibt Dir seine Entwürfe der Endpunkte dieser API zum Review, und bittet Dich, die Beispielaufrufe zu verifizieren und zu kommentieren. Erläutere für jeden Aufruf, ob er ReST-konform ist (falls nein: warum nicht), welche `CRUD`-Funktion der Aufruf übernimmt und welche Garantien die genutzte HTTP-Methode bieten sollte, um HTTP/ReST-konform zu sein. 

![Bild einer smarten Heizung. Wie die KI Dall-e sich vorstellt, dass Dali sie malen würde.](images/DALL·E%202023-11-01%2017.38.37%20-%20Ein%20von%20Salvator%20Dali%20gemaltes%20Bild%20des%20%20Raumthermostats%20einer%20Heizung,%20das%20über%20ein%20Kabel%20mit%20einem%20Router%20verbunden%20ist.webp)

Hinweis: Die Syntax des Befehls spielt hier keine Rolle. Relevant sind nur die hervorgehobenen genutzte `HTTP_METHODE`, die Werte, die als `BODY` übergeben werden (optional) sowie die aufgerufene `URL_DES_ENDPUNKS`. Der schematische Aufbau von `curl` ist folgender:

```bash
curl -X HTTP_METHODE -d BODY http://URL_DES_ENDPUNKTS
```

1.	Änderung der aktuellen Heizgrenztemperatur (auf $18.5 \rm{^{\circ}C}$) für den Raum 1:

      ```bash
      curl -X POST -d '{"raumtemp":"18.5"}' http://owl-heating.de/api/raum/1
      ```

      Wird mit diesem Aufruf eine fachgerecht entworfene ReST-Schnittstelle angesprochen? Falls nein: warum nicht? <button onclick="toggleAnswer('rest1')">Antwort</button>

      <span class="hidden-answer" id="rest1">

      Es handelt sich um einen fachgerechten ReST-Aufruf. Die HTTP-Methode `POST` kann zur Aktualisierung einer Ressource genutzt werden.

      </span>

      Die genutzte Methode dieses Aufrufs bietet folgende Garantien:<button onclick="toggleAnswer('rest2')">Antwort</button>

      <span class="hidden-answer" id="rest2">

      `POST` bietet keinerlei Garantien (ist also weder _safe_, _cachable_  noch _idempotent_).

      </span>

1.	Auslesen aller Informationen für den Raum 1:

      ```bash
      curl -X GET -d '{"raum":"1"}' http://owl-heating.de/api/raum
      ```

      Wird mit diesem Aufruf eine fachgerecht entworfene ReST-Schnittstelle angesprochen? Falls nein: warum nicht? <button onclick="toggleAnswer('rest3')">Antwort</button>

      <span class="hidden-answer" id="rest3">

      Es handelt sich um einen fehlerhaften ReST-Aufruf. Die HTTP-Methode `GET` erwartet keinen Nachrichten-Body. Bei `GET`-Requests wird die ID der Ressource über die URL übergeben. Korrekt wäre etwa

      ```bash
      curl -X GET http://owl-heating.de/api/raum/1
      ```

      </span>

      Die genutzte Methode dieses Aufrufs bietet folgende Garantien:<button onclick="toggleAnswer('rest4')">Antwort</button>

      <span class="hidden-answer" id="rest4">

      `GET`-Requests bietet alle drei Garantien, sie sind also _safe_, _cachable_  und _idempotent_.

      </span>

1.	Löschen der letzten eingegebenen Temperatur

      ```bash
      curl -X 'DELETE' http://owl-heating.de/api/temp/last
      ```

      Wird mit diesem Aufruf eine fachgerecht entworfene ReST-Schnittstelle angesprochen? Falls nein: warum nicht? <button onclick="toggleAnswer('rest5')">Antwort</button>

      <span class="hidden-answer" id="rest5">

      Es handelt sich um keinen korrekten Aufruf einer ReST-Schnittstelle. Da ReST zustandslos ist, verfügt es nicht über die Kenntnis, von welchem Zustand der Client derzeit ausgeht (anders, als bei Schnittstellen, die aktiv eine _Session_ pflegen). Daher können Ressourcen nicht über relative Verweise wie _next_, _previous_ oder _last_ angesprochen werden. 

      Ausserdem muss eine ReST-Schnittstelle den `DELETE`-Request idempotent umsetzen: mehrmaliges ausführen darf nur dieselben Seiteneffekte (Löschung) haben, wie einfaches ausführen. Würde ich immer den letzten Eintrag löschen, wären nach zwei identischen Requests auch zwei Einträge gelöscht.
      
      </span>

      Die genutzte Methode dieses Aufrufs bietet folgende Garantien:<button onclick="toggleAnswer('rest6')">Antwort</button>

      <span class="hidden-answer" id="rest6">

      `DELETE`-Requests sind _idempotent_. Die Garantien _safe_ und _cachable_ bieten sie jedoch nicht.

      </span>

1.	Ändern der Prognosewerte für die Folgewoche

      ```bash
     curl -X 'PUT' -d '{"temp":"17}' http://owl-heating.de/api/temp/2022-11-01
      ```

      Wird mit diesem Aufruf eine fachgerecht entworfene ReST-Schnittstelle angesprochen? Falls nein: warum nicht? <button onclick="toggleAnswer('rest7')">Antwort</button>

      <span class="hidden-answer" id="rest7">

      Der Aufruf ist fachgerecht. Auf diese Weise kann ein Wert aktualisiert werden.

      </span>

      Die genutzte Methode dieses Aufrufs bietet folgende Garantien:<button onclick="toggleAnswer('rest8')">Antwort</button>

      <span class="hidden-answer" id="rest8">

      `PUT`-Requests sind _idempotent_. Die Garantien _safe_ und _cachable_ bieten sie jedoch nicht.

      </span>




1. Beschreibe in einem Satz, was man im Rahmen von ReST unter "Zustandslosigkeit" versteht!<button onclick="toggleAnswer('rest9')">Antwort</button>

      <span class="hidden-answer" id="rest9">

      Alle Informationen, die zur Verarbeitung benötigt werden, werden mit dem Aufruf übergeben. Es gibt keine Session-Variablen und keinen lokalen Zustand, auf den sich die Aufrufe beziehen.

      </span>

### 3. ReST-API für einen Industrieroboter

Für einen Industrieroboter, der Produkte lackiert, soll ein ReST-Service erstellt werden. Über die ReST-Schnittstelle sollen bestimmte Positionen festgelegt und angefahren werden können. Außerdem sollen die Farbwerte darüber beeinflusst und ausgelesen werden können. Ein Fragment des Services besteht bereits, das jetzt verfeinert werden soll.

Hinweis: Die Aufrufe der ReST-API erfolgen beispielhaft über die Konsole (Windows PowerShell, das Commandlet `Invoke-WebRequest` entspricht `curl` unter Linux). Bei einem solchen Aufruf wird die Art des Requests (`HTTP_METHODE`) und die URL zur Ressource (`URL_ZU_DER_RESSOURCE`) wie in folgendem Beispiel übergeben. Bei Bedarf kann noch ein Nachrichtenpayload (`BODY_DES_REQUESTS`) und das zugehörige Datenformat (`CONTENT_TYPE_STRING`) übergeben werden:

```powershell
Invoke-WebRequest http://URL_ZU_DER_RESSOURCE 'HTTP_METHODE' -ContentType 'CONTENT_TYPE_STRING' -Body 'BODY_DES_REQUESTS'
```

![KI-generietes Bild von einem humanoiden Roboter, der lakiert.](images/DALL·E%202023-11-01%2018.46.28%20-%20Ein%20Roboter,%20der%20eine%20Farbsprühdose%20in%20der%20Hand%20hält.webp)

Die Aufgaben sind möglicherweise schwer verständlich, wenn bislang nicht mit kleinen Robotersystemen wie z.B. Dobots gearbeitet und deren API programmiert wurde. Üblich ist bei solchen Robotern, dass Positionen als dreidimensionale Koordinaten gespeichert werden (z.B. `AUFNAHMEPOS` könnte für den Wert $x=123, y=234, z=134$ stehen).

Die Art der Authentifizierung und Autorisierung spielt bei den folgenden Fragen keine Rolle.

1. Eine fertige Komponente der ReST-Schnittstelle des Roboters kann über die Powershell mit folgendem Befehl angesprochen werden:

   ```powershell
   Invoke-WebRequest http://roboter.firma.de:8085/api/position/AUFNAHMEPOS 'GET'
   ```

   Welche Reaktion oder Ausgabe erwartest Du beispielhaft vom System bei einem derartigen Aufruf? Beziehe Dich in Deiner Antwort auch auf den ReST-Begriff Repräsentation.<button onclick="toggleAnswer('restc1')">Antwort</button>

   <span class="hidden-answer" id="restc1">

   Man erwartet von dieser Art Request die Rückgabe der Werte der Position AUFNAHMEPOS in einer Repräsentationsform, z.B. als JSON:
   
   ```json
   {‘X‘:80, ‘Y‘:30, ‘Z‘:12}}
   ```

   gegebenenfalls (wenn auch weniger üblich) als CSV, XML oder beliebiger anderer Form, die die API als Standard implementiert hat.
   
   </span>

1. Eine Kollegin / ein Kollege hat den folgenden Endpunkt programmiert, mit dem der Roboterarm zu einer benannten und gespeicherten Stelle bewegt werden kann. Er/Sie führt dir vor, wie praktisch man nun den Roboterarm positionieren kann (nach diesem Request bewegt sich der Arm auf die gewünschte Position):

   ```powershell
   Invoke-WebRequest http://roboter.firma.de:8085/api/move_to/LACKIERPOS 'GET'
   ```

   Bewerte den Vorschlag hinsichtlich der Grundlagen von ReST-Schnittstellen und schlage ggf. eine Alternative vor. <button onclick="toggleAnswer('restc2')">Antwort</button>

   <span class="hidden-answer" id="restc2">

   `GET`-Requests müssen grundsätzlich _safe_ sein, dürfen das System also nicht verändern. Durch die Bewegung des Arm verändert sich der Zustand des Systems aber nachhaltig. Da es sich um eine Änderung des Zustands handelt (`UPDATE`) wäre `PUT` das korrekte HTTP-Verb für den Request. Allerdings sollte der neue Zustand im Body übergeben werden, da die URL nur die Ressource anspricht. Denkbar wäre ein Request wie:

    ```powershell
   Invoke-RestMethod http://roboter.firma.de:8085/api/current_position/
   -Method 'PUT' 
   -Body '{"name":"LACKIERPOS"}'
   ```

   </span>

1. Beschreibe, welche Reaktion des Systems Du beim folgenden Aufruf erwartest: 

   ```powershell
   Invoke-RestMethod http://roboter.firma.de:8085/api/farbsaettigung 
   -Method 'PUT' 
   -ContentType 'application/json; charset=utf-8' 
   -Body '{"min":60, "max":80}'
   ```

   <button onclick="toggleAnswer('restc3')">Antwort</button>

   <span class="hidden-answer" id="restc3">

   Die Ressource farbsaettigung wird hier geänder (geupdatet) und hat fortan einen neuen Wert.

   </span>

1. Beschreibe, welche Reaktion des Systems Du beim folgenden Aufruf erwartest: 

   ```powershell
   Invoke-RestMethod http://roboter.firma.de:8085/api/position/
   -Method 'POST' 
   -ContentType 'application/json; charset=utf-8' 
   -Body '{"name":"RUHEPOS", "x":60, "y":80, "z":80}'
   ```

   <button onclick="toggleAnswer('restc4')">Antwort</button>

   <span class="hidden-answer" id="restc4">

   Es wird eine neue Ressource erzeugt, die die übergebenen Parameter hat.

   </span>

1. HTTP-Methoden bieten unterschiedliche Garantien/Eigenschaften. Nenne die drei wesentlichen Garantien und ordne zu, welche Garantien die Aufrufe oben von `GET` `PUT` und `POST` bieten

   <button onclick="toggleAnswer('restc5')">Antwort</button>

   <span class="hidden-answer" id="restc5">

   |         |  Cacheable   | Idempotent | Safe
   |---|---|---|---|
   | GET   |     ja       | ja         | ja   |
   | PUT   |   nein       | ja         | nein   |
   | POST  |     nein       | nein       | nein   |

   </span>

1. Was versteht man unter einer URI? Nenne beispielhaft eine URI der Lacker-Roboter-ReST-API!

   <button onclick="toggleAnswer('restc6')">Antwort</button>

   <span class="hidden-answer" id="restc6">

   Mit einer URI wird eine Ressource (eine Entität der API) eindeutig identifiziert. Die oben beschriebene API verwendet als URI URLs wie `http://roboter.firma.de:8085/api/position/RUHEPOS`

   </span>

### Links und weitere Informationen

* Wer es genau wissen will: die ReST Architektur basiert auf einer Dissertation von Roy Thomas Fielding mit dem Titel [Architectural Styles and
the Design of Network-based Software Architectures](https://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm)

* Hauptquelle für Informationen zu ReST ist die HTTP1.1 Spezifikation, festgeschrieben in der [RFC-2616 (link)](https://www.ietf.org/rfc/rfc2616.txt)
